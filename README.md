<p align="center">
  <img src="https://www.linkace.org/images/linkace-social.jpg" title="Linkace">
</p>

<p align="center"><b>Your self-hosted bookmark archive.</b></p>

<p align="center">
  <a href="https://twitter.com/LinkAceApp"><img src="https://img.shields.io/twitter/url?label=%40LinkAceApp&style=social&url=https%3A%2F%2Ftwitter.com%2FLinkAceApp" alt="Follow LinkAce on Twitter"></a>
  <a href="https://hub.docker.com/r/linkace/linkace"><img src="https://img.shields.io/badge/Docker-linkace%2Flinkace-2596EC.svg" alt="Docker Repository"></a>
  <a href="https://github.com/Kovah/LinkAce/releases"><img src="https://img.shields.io/github/v/release/kovah/linkace?label=Latest%20Release" alt="Latest Release"></a>
  <a href="https://opensource.org/licenses/GPL-3.0"><img src="https://img.shields.io/github/license/kovah/linkace.svg" alt="License"></a>
</p>
<p>&nbsp;</p>


## Edit the base configuration

You have to change the following settings in the .env file before starting the setup:

```
DB_PASSWORD - Please set a secure password here
REDIS_PASSWORD - Please set a secure password here
Add SESSION_DRIVER=redis and CACHE_DRIVER=redis as separate lines to the file
```

If you are unsure if the .env file is writable inside Docker, please make it writable for anybody (-rw-rw-rw- or 666). You can switch back to make it read only after the setup.
